<?php
class User
{
    private $id;
    private $username;
    private $password;
    private $role;

    function __construct($id, $username, $password, $role)
    {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
        $this->role = $role;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getRole()
    {
        return $this->role;
    }

    function getPassword()
    {
        return $this->password;
    }

    function __toString()
    {
        return $this->username;
    }
}